
package modelo;
import java.sql.*;

/**
 *
 * @author quier
 */
public class DbCotizacion extends DbManejador implements dbPersistencia{
    
    @Override
    public void insertar(Object obj) throws Exception {
        conectar();
        
        Cotizaciones cot = new Cotizaciones();
        cot = (Cotizaciones) obj;
        
        try {
            sqlConsulta = "INSERT INTO "
                    + "cotizaciones(numCotizacion,descripcion,precio,porcentaje,plazos)"
                    + " VALUES (?,?,?,?,?)";
            
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            sqlComando.setString(1, cot.getNumCotizacion());
            sqlComando.setString(2, cot.getDescripcionAutomovil());
            sqlComando.setInt(3, cot.getPrecioAutomovil());
            sqlComando.setInt(4, cot.getPorcentajePago());
            sqlComando.setInt(5, cot.getPlazo());
            
            sqlComando.executeUpdate();
        }
        catch(SQLException e) {
            System.out.println("Error al insertar: " + e.getMessage());
        }
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void actualizar(Object obj) throws Exception {
        conectar();
        
        Cotizaciones cot = new Cotizaciones();
        cot = (Cotizaciones) obj;
        
        try {
            sqlConsulta = "UPDATE cotizaciones SET descripcion = ?, precio = ?, "
                    + " porcentaje = ?, plazos = ? WHERE numCotizacion = ?";
            
            //System.out.println(sqlConsulta);
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            
            sqlComando.setString(1, cot.getDescripcionAutomovil());
            sqlComando.setInt(2, cot.getPrecioAutomovil());
            sqlComando.setInt(3, cot.getPorcentajePago());
            sqlComando.setInt(4, cot.getPlazo());
            sqlComando.setString(5, cot.getNumCotizacion());
            
            sqlComando.executeUpdate();
        }
        catch(SQLException e) {
            System.out.println("Error al Actualizar: " + e.getMessage());
        }
    }

    @Override
    public void borrar(Object obj) throws Exception {
        conectar();
        
        Cotizaciones cot = new Cotizaciones();
        cot = (Cotizaciones) obj;
        
        try {
            sqlConsulta = "DELETE FROM cotizaciones "
                    + "WHERE numCotizacion = ?";
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            
            sqlComando.setString(1, cot.getNumCotizacion());
            sqlComando.executeUpdate();
        }
        catch(SQLException e) {
            System.out.println("Error al Borrar: " + e.getMessage());
        }
        desconectar();
    }

    @Override
    public Object consultar(String codigo) throws Exception {
        Cotizaciones cotiza = new Cotizaciones();
        conectar();
        
        try {
            sqlConsulta = "SELECT * from cotizaciones where numCotizacion = ?";
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            
            sqlComando.setString(1, codigo);
            this.registros = sqlComando.executeQuery();
            
            if(registros.next())
            {
                cotiza.setIdCotizacion(registros.getInt("idCotizacion"));
                cotiza.setDescripcionAutomovil(registros.getString("descripcion"));
                cotiza.setPrecioAutomovil(registros.getInt("precio"));
                cotiza.setPorcentajePago(registros.getInt("porcentaje"));
                cotiza.setPlazo(registros.getInt("plazos"));
            }
            else cotiza.setIdCotizacion(-1);
        }
        catch(SQLException e) {
            System.out.println("Error al consultar: " + e.getMessage());
        }
        desconectar();
        return cotiza;
    }
    
    @Override
    public int numRegistros() {
        int numRegistros = 0;
        conectar();

        try {
            sqlConsulta = "SELECT * FROM cotizaciones";
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            registros = sqlComando.executeQuery();

            while (registros.next()) {
                ++numRegistros;
            }
        } catch (SQLException e) {
            System.out.println("Error al consultar: " + e.getMessage());
        }

        desconectar();
        return numRegistros;
    }
    
    @Override
    public Object[][] cargarDatos() {
        int numRegistros = this.numRegistros();
        Object datos[][] = new Object[numRegistros][6];
        conectar();
        
        try {
            sqlConsulta = "SELECT * FROM cotizaciones ORDER BY idCotizacion";
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            registros = sqlComando.executeQuery();
            int con = 0;
            
            while(registros.next()) {
                // Por cada registro mete el valor que le corresponde al campo de la tabla
                // Este ciclo es para leer todos los registros que tiene el cursor
                datos[con][0] = registros.getInt("idCotizacion");
                datos[con][1] = registros.getInt("numCotizacion"); // int????
                datos[con][2] = registros.getString("descripcion");
                datos[con][3] = registros.getInt("precio");
                datos[con][4] = registros.getInt("porcentaje");
                datos[con][5] = registros.getInt("plazos");
                con++;
            }
        }
        catch(SQLException e) {
            System.out.println("Error al consultar" + e.getMessage());
        }
        desconectar();
        return datos;
    }
    
    @Override
    public boolean isExiste(String codigo) {
        Cotizaciones cot = new Cotizaciones();
        
        try {
            this.conectar();
            String consulta = "SELECT * FROM cotizaciones WHERE numCotizacion = ?";
            PreparedStatement sqlComando = this.conexion.prepareStatement(consulta);
            //System.out.println(codigo);
            
            // Asignar valores
            sqlComando.setString(1, codigo);

            // Hacer la consulta
            this.registros = sqlComando.executeQuery();

            // Se checa si el registro del código existe
            if(this.registros.next()) {
                if (this.registros.getString("numCotizacion") != null)
                    return true;
                else
                    return false;
            }
        }
        catch(Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return false;
    }
}
